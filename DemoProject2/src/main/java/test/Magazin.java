package test;

import java.util.concurrent.ThreadLocalRandom;

public class Magazin extends Thread {
	String s = "";
	Casa casa[];
	String servire = "";
	String plasare = "";
	private int nr_case;
	static long ID = 0;
	private int nr_clienti;// Numar calatori care trebuie deserviti de catre casele de bilete

	public Magazin(int nr_case, Casa casa[], String name, int nr_clienti) {
		setName(name);
		this.nr_case = nr_case;
		this.casa = new Casa[nr_case];
		this.nr_clienti = nr_clienti;
		for (int i = 0; i < nr_case; i++) {
			this.casa[i] = casa[i];
		}
	}

	private int min_index() {// se cauta casa cu cei mai putini oameni
		int coada = 0;
		try {
			long minim = casa[0].lungime_coada();
			for (int i = 1; i < nr_case; i++) {
				long lung = casa[i].lungime_coada();
				if (lung < minim) {
					minim = lung;
					coada = i;
				}
			}
		} catch (InterruptedException e) {
			System.out.println(e.toString());
		}
		return coada;
	}

	public synchronized void run() {// 2 de aici
		try {
			int i = 0;
			while (i < nr_clienti) {// i < nr clienti = se adauga toti clientii la coada minima
				i++;
				//se creeaza un now client
				Client c = new Client(++ID, ThreadLocalRandom.current().nextInt(2, 6));// 3 creare client
				int m = min_index();
				plasare += "\nClient :" + Long.toString(ID) + " adaugat la CASA " + Integer.toString(m) + " cu timpul de asteptare : " + c.timp;

				casa[m].adauga_client(c);// la casa minima se adauga clientul c si se apeleaza metoda din clasa CASA
				sleep(1500);
			}
			while (true) {
				for (int j = 0; j < casa.length; j++) {
					synchronized (casa[j]) {
						s = casa[j].print_client();
						servire = casa[j].servire;
					}
					if (casa[j].lungime_coada() == 0)
						System.out.println("Coada inchisa");

				}
			}
		} catch (InterruptedException e) {
			System.out.println(e.toString());
		}
	}
}
