package test;
public class Client {

	long cID;
	int timp = 0;

	public Client(long cID, int ttimp) {
		this.cID = cID;
		this.timp = ttimp;
	}

	public long getCID() {
		return cID;
	}

	public String toString() {
		return (Long.toString(cID) + " ");
	}
}