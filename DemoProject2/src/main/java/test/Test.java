package test;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Test{

	
	JProgressBar pBar = new JProgressBar();
	JFrame window;
	JTextArea log = new JTextArea();
	JScrollPane scroll = new JScrollPane(log);
	JButton test = new JButton("ceva");
	JTextField test0 = new JTextField();
	JTextField test1 = new JTextField();
	JTextField test2 = new JTextField();
	
	Font font = new Font("Times New Roman", Font.BOLD,20);
	//Timer t;
	int count = 0;
	int a;

	public Test(int queue,int clienti) {

		int i;
		final int nr = queue;
		int nr_clienti = clienti;
		
		//if(nr_clienti > 6)
		//	nr++;
		
		Casa c[] = new Casa[nr];
		for (i = 0; i < nr; i++) {
			c[i] = new Casa("Casa " + Integer.toString(i));
			c[i].start();
		}
		
		final Magazin m = new Magazin(nr, c, "Magazin", nr_clienti);// 1 creare producator
		m.start();

		window = new JFrame("Test");
		window.setSize(1000, 600);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.getContentPane().setBackground(Color.gray);
		window.setLayout(null);

		log.setBounds(10, 10, 600, 550);
		log.setBackground(Color.gray);
		log.setFont(font);
		log.setEditable(false);
		log.setText("ceva");
		scroll.setBounds(10,10,600,550);
		scroll.setBackground(Color.GRAY);
		scroll.setFont(font);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JLabel casa0 = new JLabel("Casa 0");
		
		casa0.setBounds(700,25,100,20);
		casa0.setForeground(Color.black);
		test0.setBounds(650, 50, 300, 50);
		test0.setForeground(Color.BLACK);
		test0.setFont(font);
		test0.setEditable(false);
		test0.setText("inchisa");
		//pBar.setMinimum(0);
		//pBar.setMaximum(100);
		//pBar.setBounds(650,100,300,20);
		
		
		JLabel casa1 = new JLabel("Casa 1");
		
		casa1.setBounds(700,125,100,20);
		casa1.setForeground(Color.black);
		test1.setBounds(650, 150, 300, 50);
		test1.setForeground(Color.BLACK);
		test1.setFont(font);
		test1.setEditable(false);
		test1.setText("inchisa");
		//pBar.setMinimum(0);
		//pBar.setMaximum(100);
		//pBar.setBounds(650,00,300,20);
		
		JLabel casa2 = new JLabel("Casa 2");
		
		casa2.setBounds(700,225,100,20);
		casa2.setForeground(Color.BLACK);
		test2.setBounds(650,250,300,50);
		test2.setForeground(Color.black);
		test2.setFont(font);
		test2.setEditable(false);
		test2.setText("inchisa");
		
		 

		  Timer time = new Timer(); 
		  TimerTask task = new TimerTask() { 
			  public void run() { 
				  count++;
				  test0.setText("" + count); 
				  test0.setText(m.s);
					try {
						a = m.casa[0].timp_maxim();
						System.out.println("AICI"+a);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					
					
					
				  	try {
				  		
				  		/////////////////////////////////////////////
							pBar.setValue(a);
							a--;
						///////////////////////////////////////
				  		
				  		test0.setText(m.casa[0].print_client()); 
				  		m.plasare += m.casa[0].servire;
				  		log.setText(m.plasare);
				  		m.casa[0].servire = "";
				  		
				  		if(nr >= 2) {
				  		test1.setText(m.casa[1].print_client());//nu printeaza ultimii 2 ramasi crapa la ultimele 2
				  		m.plasare += m.casa[1].servire;
				  		m.casa[1].servire = "";
				  		}
				  		
				  		if(nr >= 3) {
				  		test2.setText(m.casa[2].print_client());
				  		m.plasare += m.casa[2].servire;
				  		m.casa[2].servire = "";
				  		}
				  		} 
				  catch (InterruptedException e)
				  	{
					  e.printStackTrace();
				  	}
				  } 
			  };
		  
		  time.scheduleAtFixedRate(task, 1000, 1000);

		  int temp = 0;
		  for(i = 0; i < nr; i++)
		  {
			  try {
				if(temp <= m.casa[i].timp_maxim())
				  temp = m.casa[i].timp_maxim();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		  }
		  //log.append("\n\n Casa cu timpul cel mai lung este : " + " de " + temp +" secunde");
		  //p.plasare += temp;
		window.add(test0);
		window.add(test1);
		window.add(test2);
		window.add(pBar);
		window.add(scroll);
		window.add(casa0);
		window.add(casa1);
		window.add(casa2);
		//window.add(history);

		window.setVisible(true);
	}

	public static void start() {
		JFrame window;
		JButton test = new JButton();
		final JTextField nr_clienti = new JTextField();
		final JTextField nr_cozi = new JTextField();
		JLabel client = new JLabel("nr_clienti");
		JLabel cozi = new JLabel("cozi");
		
		window = new JFrame("Test");
		window.setSize(500, 300);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.getContentPane().setBackground(Color.gray);
		window.setLayout(null);
		
		client.setBounds(100,50,70,30);
		client.setForeground(Color.black);
		cozi.setBounds(100,100,70,30);
		cozi.setForeground(Color.black);

		
		nr_clienti.setBounds(180,50,100,30);
		nr_cozi.setBounds(180,100,100,30);
		
		
		test.setBounds(190,10,90,20);
		test.setText("Run Sim");
		test.setForeground(Color.black);
		test.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new Test(Integer.parseInt(nr_cozi.getText()),Integer.parseInt(nr_clienti.getText()));
			}
		});
		
		window.add(client);
		window.add(cozi);
		window.add(nr_clienti);
		window.add(nr_cozi);
		window.add(test);
		window.setVisible(true);
	}

	public static void main(String args[]) {
		//new TestGara();
		start();
	}
}