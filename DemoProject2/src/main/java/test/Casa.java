package test;
import java.util.ArrayList;
import java.util.Vector;

public class Casa extends Thread {
	Vector clienti = new Vector();
	ArrayList<Client> client = new ArrayList<Client>();
	String tmp = "";
	String servire = "";
	int max_size = 5;

	public Casa(String name) {
		setName(name);
	}

	public void run() {
		try {
			while (true) {
				sterge_client();
				sleep(2000);

			}
		} catch (InterruptedException e) {
			System.out.println("Intrerupere");
			System.out.println(e.toString());
		}
	}

	public synchronized String print_client() throws InterruptedException {

		tmp = " ";
		for (int k = 0; k < client.size(); k++) {// client id + timp asteptare (print)

			if (client.get(k).timp <= 0) {
				client.remove(k);
			} else {
				// asta printeaza timpii ramasi
				tmp += client.get(k).timp;
				tmp += "(" + client.get(k).cID + ")  ";

			}
		}
		sleep(10);//was 10//was 100
		System.out.println();
		return tmp;
	}

	
	public synchronized int timp_maxim() throws InterruptedException
	{
		int a = 0;
		
		for(int i = 0; i < client.size(); i++)
			a += client.get(i).timp;
		
		return a;
	}
	public synchronized void sterge_client() throws InterruptedException {
		//decrementeaza timpul tuturor clientilor cu 1
		while (client.size() == 0)
			wait();
		for (int i = 0; i < client.size(); i++) {
			client.set(i, new Client(client.get(i).getCID(), client.get(i).timp - 1));
			if(client.get(i).timp == 0)
				servire += "\nClientul " + client.get(i).cID + " a fost servit de casa " + getName();
		}
		Thread.sleep(100);//was 100
	}

	public synchronized void adauga_client(Client c) throws InterruptedException {
		//adauga clienti in arrayList
		clienti.addElement(c);
		client.add(c);// la adaugare ar trebui adaugat + timpii celor din fata la timpul curent
		for (int i = 0; i < client.size() - 1; i++)
			client.get(client.size() - 1).timp += client.get(i).timp;
		notifyAll();

	}

	public synchronized long lungime_coada() throws InterruptedException {
		//calculeaza lungimea cozii
		notifyAll();
		long size = clienti.size();
		return size;
	}
}
